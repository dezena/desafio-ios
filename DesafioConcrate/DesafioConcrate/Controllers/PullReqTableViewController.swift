//
//  PullReqTableViewController.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 25/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import KDLoadingView

class PullReqTableViewController: UITableViewController {
    
    var repository : Items!
    var pullRequests : [PROfRepository] = []
    
    
    var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(named: "main")
        return label
    }()

    override func viewDidLoad() {
        
        label.text = "Carregando Pull Requests"
        super.viewDidLoad()
        
        self.title = repository.name

        Rest.loadPullRequest(creator: repository.owner.login, repository: repository.name, onComplete: { (pullRequests) in
            self.pullRequests = pullRequests
            DispatchQueue.main.async {
                self.label.text = "Nenhum pull request encontrado."
                self.tableView.reloadData()
            }
        }) { (error) in
            print(error)
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        tableView.backgroundView = pullRequests.count == 0 ? label : nil
        return pullRequests.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullReqTableViewCell") as! PullReqTableViewCell
        let pullRequest = pullRequests[indexPath.row]
        cell.prepare(with: pullRequest)

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        if let url = URL(string: pullRequest.html_url) {
            UIApplication.shared.open(url, options: [:])
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }

}
