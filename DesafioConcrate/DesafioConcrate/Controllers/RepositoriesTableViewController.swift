//
//  RepositoriesTableViewController.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 25/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import KDLoadingView

class RepositoriesTableViewController: UITableViewController {
    
    @IBOutlet weak var loadingView: KDLoadingView!
    
    var items : [Items] = []
    var pullRequests : [PROfRepository] = []
    var totalItems : Int = 0
    var pageNumber : Int = 1
    
    var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(named: "main")
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.text = "Carregando repositorios..."

        Rest.loadRepositories(pageNumber: pageNumber, onComplete: { (repositories) in
            self.items = repositories.items
            self.totalItems = self.items.count
            self.pageNumber += 1

            DispatchQueue.main.sync {
                self.tableView.reloadData()
                self.loadingView.startAnimating()
            }
        }) { (error) in
            print(error)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.backgroundView = items.count == 0 ? label : nil
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryTableViewCell") as! RepositoryTableViewCell
        
        let repository = items[indexPath.row]
        cell.prepare(with: repository)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if totalItems - 2 == indexPath.row {
            if pageNumber < 35 {
                Rest.loadRepositories(pageNumber: pageNumber, onComplete: { (repositories) in
                    self.items.append(contentsOf: repositories.items)
                    self.totalItems += repositories.items.count
                    self.pageNumber += 1
                    DispatchQueue.main.sync {
                        self.tableView.reloadData()
                    }
                }) { (error) in
                    print(error)
                }
            }else{
                print("fim")
                self.loadingView.stopAnimating()

            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         let vc = segue.destination as! PullReqTableViewController
         let repository = items[tableView.indexPathForSelectedRow!.row]
         vc.repository = repository
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    
}
