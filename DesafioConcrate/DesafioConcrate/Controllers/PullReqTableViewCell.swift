//
//  PullReqTableViewCell.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 25/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import SDWebImage

class PullReqTableViewCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbBody: UILabel!
    @IBOutlet weak var imUser: UIImageView!
    @IBOutlet weak var lbNameUser: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func prepare(with pullRequest: PROfRepository){
        lbTitle.text = pullRequest.title
        lbBody.text = pullRequest.body
        imUser.sd_setImage(with: URL(string: pullRequest.user.avatar_url))
        lbNameUser.text = pullRequest.user.login
        lbDate.text = pullRequest.created_at
    }

}
