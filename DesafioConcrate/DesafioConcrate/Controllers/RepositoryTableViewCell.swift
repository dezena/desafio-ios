//
//  RepositoryTableViewCell.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 25/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lbNameRep: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbFork: UILabel!
    @IBOutlet weak var lbStar: UILabel!
    @IBOutlet weak var imAvatar: UIImageView!
    @IBOutlet weak var lbNameUser: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.layer.cornerRadius = 10
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func prepare(with repository: Items) {
        lbFork.text = String(repository.forks)
        lbDescription.text = repository.description
        lbNameRep.text = repository.name
        lbNameUser.text = repository.owner.login
        lbStar.text = String(repository.watchers)
        imAvatar.sd_setImage(with: URL(string: repository.owner.avatar_url))
    }
    
}
