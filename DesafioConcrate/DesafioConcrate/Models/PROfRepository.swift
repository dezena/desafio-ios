//
//  PROfRepository.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 24/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

struct PROfRepository: Decodable {
    
    var title : String? // title
    var user : User // user
    var body : String? // body
    var created_at : String // created_at format "2018-01-24T13:35:41Z"
    var html_url : String

}
