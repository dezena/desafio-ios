//
//  User.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 24/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

struct User : Decodable {
    var login : String // name
    var avatar_url : String // image
}


