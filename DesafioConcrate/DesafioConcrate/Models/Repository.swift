//
//  Repository.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 24/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

struct Repository : Decodable{
    
    var items : [Items]
    
}

struct Items : Decodable{
    var name : String
    var description : String? // description
    var forks : Int // forks
    var watchers : Int // stars
    var owner : User // owner
    
}
