//
//  Rest.swift
//  DesafioConcrate
//
//  Created by Gabriel Dezena on 24/01/18.
//  Copyright © 2018 Gabriel Dezena. All rights reserved.
//

import Foundation

enum RestError {
    case url
    case noResponse
    case noData
    case responseStatusCode(code: Int)
    case invalidJson
}

class Rest{
    
    private static let session = URLSession.shared

    class func loadRepositories (pageNumber:Int, onComplete: @escaping (Repository) -> Void, onError: @escaping (RestError) -> Void) {
        guard let url = URL(string: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(pageNumber)") else {
            onError(.url)
            return
        }
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil{
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                if response.statusCode == 200 {
                    guard let data = data else {
                        onError(.noData)
                        return
                    }
                    do{
                        let repositories  = try JSONDecoder().decode(Repository.self, from: data)
                        onComplete(repositories)
                    } catch{
                        onError(.invalidJson)
                        print(error.localizedDescription)
                    }
                }else {
                    onError(.responseStatusCode(code: response.statusCode))
                }
            }else{
                print(error!)
            }
        }
        dataTask.resume()
    }
    
    class func loadPullRequest (creator:String, repository: String, onComplete: @escaping ([PROfRepository]) -> Void, onError: @escaping (RestError) -> Void) {
        guard let url = URL(string: "https://api.github.com/repos/\(creator)/\(repository)/pulls") else {
            onError(.url)
            return
        }
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil{
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                if response.statusCode == 200 {
                    guard let data = data else {
                        onError(.noData)
                        return
                    }
                    do{
                        let pullRequests  = try JSONDecoder().decode([PROfRepository].self, from: data)
                        onComplete(pullRequests)
                    } catch{
                        onError(.invalidJson)
                        print(error.localizedDescription)
                    }
                }else {
                    onError(.responseStatusCode(code: response.statusCode))
                }
            }else{
                print(error!)
            }
        }
        dataTask.resume()
    }
}

